# The VPC

resource "aws_vpc" "vpc_main" {
cidr_block           = "${var.vpc_cidr}"
enable_dns_hostnames = true
enable_dns_support   = true

tags = {
Name        = "${var.environment}-vpc"
Environment = "${var.environment}"
}
}
# Internet gateway for the public subnet
resource "aws_internet_gateway" "ig" {
vpc_id = "${aws_vpc.vpc_main.id}"

tags = {
Name        = "${var.environment}-igw"
Environment = "${var.environment}"
}
}

/* Elastic IP for NAT */
resource "aws_eip" "nat_eip" {
vpc        = true
depends_on = [aws_internet_gateway.ig]
}

# NAT gateway
resource "aws_nat_gateway" "nat_gw" {
allocation_id = "${aws_eip.nat_eip.id}"
subnet_id     = "${element(aws_subnet.public_subnet.*.id, 0)}"
depends_on    = [aws_internet_gateway.ig]

tags = {
Name        = "nat"
Environment = "${var.environment}"
}
}

# Public subnet
resource "aws_subnet" "public_subnet" {
vpc_id                  = "${aws_vpc.vpc_main.id}"
count                   = "${length(var.public_subnets_cidr)}"
cidr_block              = "${element(var.public_subnets_cidr, count.index)}"
availability_zone       = "${element(var.availability_zones, count.index)}"
map_public_ip_on_launch = true

tags = {
  Name        = "${var.environment}-${element(var.availability_zones, count.index)}-public-subnet"
  Environment = "${var.environment}"
  "kubernetes.io/cluster/app-cluster" = "shared"
  "kubernetes.io/role/elb" = "1"
}
}

# Private subnet
resource "aws_subnet" "private_subnet" {
vpc_id                  = "${aws_vpc.vpc_main.id}"
count                   = "${length(var.private_subnets_cidr)}"
cidr_block              = "${element(var.private_subnets_cidr, count.index)}"
availability_zone       = "${element(var.availability_zones, count.index)}"
map_public_ip_on_launch = false

tags = {
  Name        = "${var.environment}-${element(var.availability_zones, count.index)}-private-subnet"
  Environment = "${var.environment}"
  "kubernetes.io/cluster/app-cluster" = "shared"
  "kubernetes.io/role/internal-elb"  = "1"
}
}

# Routing table - private subnet
resource "aws_route_table" "private" {
vpc_id = "${aws_vpc.vpc_main.id}"

tags = {
Name        = "${var.environment}-private-route-table"
Environment = "${var.environment}"
}
}

# Routing table - public subnet
resource "aws_route_table" "public" {
vpc_id = "${aws_vpc.vpc_main.id}"

tags = {
Name        = "${var.environment}-public-route-table"
Environment = "${var.environment}"
}
}

resource "aws_route" "public_internet_gateway" {
route_table_id         = "${aws_route_table.public.id}"
destination_cidr_block = "0.0.0.0/0"
gateway_id             = "${aws_internet_gateway.ig.id}"
}

resource "aws_route" "private_nat_gateway" {
route_table_id         = "${aws_route_table.private.id}"
destination_cidr_block = "0.0.0.0/0"
nat_gateway_id         = "${aws_nat_gateway.nat_gw.id}"
}

/* Route table associations */
resource "aws_route_table_association" "public" {
count          = "${length(var.public_subnets_cidr)}"
subnet_id      = "${element(aws_subnet.public_subnet.*.id, count.index)}"
route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "private" {
count          = "${length(var.private_subnets_cidr)}"
subnet_id      = "${element(aws_subnet.private_subnet.*.id, count.index)}"
route_table_id = "${aws_route_table.private.id}"
}

# VPC's Default Security Group
resource "aws_security_group" "basic" {
name        = "${var.environment}-default-sg"
description = "Default security group to allow inbound/outbound from the VPC"
vpc_id      = "${aws_vpc.vpc_main.id}"
depends_on  = [aws_vpc.vpc_main]

ingress {
from_port = "80"
to_port   = "80"
protocol  = "TCP"
self      = true
}

egress {
from_port = "0"
to_port   = "0"
protocol  = "-1"
self      = "true"
}

tags = {
Environment = "${var.environment}"
}
}


data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.11"
}

//data "aws_availability_zones" "available" {
//}

locals {
  cluster_name = "app-cluster"
}

data "aws_subnet_ids" "subnet_ids" {
  vpc_id = aws_vpc.vpc_main.id
}
data "aws_subnet" "subnet" {
  count = "${length(data.aws_subnet_ids.subnet_ids.ids)}"
  id    = "${tolist(data.aws_subnet_ids.subnet_ids.ids)[count.index]}"
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "12.2.0"

  cluster_name    = "${local.cluster_name}"
  cluster_version = "1.18"
  subnets         = "${data.aws_subnet.subnet.*.id}"

  worker_ami_name_filter_windows = "*"

  vpc_id = aws_vpc.vpc_main.id

  node_groups = {
    first = {
      desired_capacity = 2
      max_capacity     = 3
      min_capacity     = 1

      instance_type = "t2.micro"
    }
  }

  write_kubeconfig   = true
  config_output_path = "./"
}