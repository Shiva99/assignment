provider "aws" {
  region                  = var.region
  shared_credentials_file = "/Users/ramakrishnanandakaridas/.aws/credentials"
  profile = "prod"
}