
//AWS
region      = "eu-central-1"
environment = "prod"

/* module networking */
vpc_cidr             = "10.0.0.0/16"
public_subnets_cidr  = ["10.0.0.0/26","10.0.0.64/26"] //List of Public subnet cidr range
private_subnets_cidr = ["10.0.0.128/26","10.0.0.192/26"] //List of private subnet cidr range

availability_zones   = ["eu-central-1a","eu-central-1b","eu-central-1c"]