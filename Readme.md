This Project creates simple network on AWS with 1 vpc, 4 subnets, 1 internet gateway, 1 NAT gateway, aws routes and associations.

Terraform is chosen for infrastructure as code automation, its flexible, used largely by the community. 

Simple HTML + javascript app that does factorial of non negative integer because its simple to generate on the client side.

The application containersed with docker and runs on eks cluster deployed in subnets and their vpc.

There is a loadbalancer service that exposes application. IAM with uname and password and admin policies created manually.

App can be reached with loadbalacer endpoint below at port 80.
  af50840a3abab426c90044cc7ca81512-140800425.eu-central-1.elb.amazonaws.com:80